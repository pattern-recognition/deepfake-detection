# Hello this is the Python Notebook of us
# Imported libraries
import glob
import os
import cv2
import urllib
import json
import torch
import numpy as np
from PIL import Image

import multiprocessing as mp
pool = mp.Pool(mp.cpu_count()-1)
print("Number of processors: ", mp.cpu_count())

fileslist = glob.glob('./input/*.mp4')
flen = len(fileslist)
validation_threshold = flen - (flen / 5)

def run():
  counter = 0
  for f in fileslist:
    #p = mp.Process(target=doImage, args=(str(f), counter, 0, ))
    #p.start()
    doImage(f, counter, 0)
    counter += 1



device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
print(f'Running on device: {device}')

with open('./metadata.json') as json_file:
  files = json.load(json_file)


def getFakestFrames(video_path, fake_path, threshold, i):
    video = cv2.VideoCapture(video_path)
    original = cv2.VideoCapture(fake_path)
    
    max_difference = 0
    fakest_frame = None
    while video.isOpened():
        ret, frame = video.read()
        ret_og, frame_og = original.read()
        if ret == True:
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame_og = cv2.cvtColor(frame_og, cv2.COLOR_BGR2GRAY)

            ret_diff, difference = cv2.threshold(cv2.subtract(frame_gray, frame_og), threshold, 255, cv2.THRESH_BINARY)

            difference_coefficient = np.mean(difference)
            if difference_coefficient > max_difference:
              cv2.imwrite( "./single_output/difference.jpg", difference)
              fakest_frame = frame
        else:
          break

    return fakest_frame

def getFrame100(url):
  vidcap = cv2.VideoCapture(url)
  count = 1
  success = True
  while success and count <= 100:
    success,image = vidcap.read()
    if count%100 == 0:
      return image
    count += 1


def take5Frames(url, i):
  vidcap = cv2.VideoCapture(url)
  count = 1
  succes = True
  while succes and count <=  ((i + 1) * 30):
    succes, image = vidcap.read()
    if count%((i*30) + 30) == 0:
      return image
    count += 1

import cv2
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import face_recognition
from mtcnn import MTCNN

def preprocessImage(frame):
  face_locations = face_recognition.face_locations(frame)

  if len(face_locations) > 0:
    face_location = face_locations[0]
    # Print the location of each face in this image
    top, right, bottom, left = face_location
    # You can access the actual face itself like this:
     
    face_image = frame[top:bottom, left:right]
    '''
    fig, ax = plt.subplots(1,1, figsize=(5, 5))
    face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
    plt.grid(False)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.imshow(face_image)
    '''
    face_image = cv2.resize(face_image, (300, 300)) 
    return face_image
  else:
    # Create face detector
      #mtcnn = MTCNN(margin=40, select_largest=False, post_process=False, device='cuda:0')
    
    # Detect face
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    detector = MTCNN()
    #frame = Image.fromarray(frame)
    face = detector.detect_faces(frame)
    if len(face) > 0:
      x = face[0]['box'][0]
      y = face[0]['box'][1]
      width = face[0]['box'][2]
      height= face[0]['box'][3]
      paddingx = 0
      paddingy = 0
      if width > height:
        paddingy = int((width - height) / 2)
      else:
        paddingx = int((height - width) / 2)
      face_image = frame[(y-paddingy):(y+height+paddingy), (x-paddingx):(x+width+paddingx)]
      h, w, channels = face_image.shape
      face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
    else:
      face_image = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    # Visualize
    '''
    fig, ax = plt.subplots(1,1, figsize=(5, 5))
    plt.grid(False)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.imshow(face_image)
    '''
    face_image = cv2.resize(face_image, (300, 300)) 
    return face_image

#Create folder 'input' containing mp4 files
def doImage(f, counter, i):
  if i != 0:
    if i < 5:
      doImage(f, counter, i+1)
    frame = take5Frames(f, i)
    label = "REAL"
  else:
    print(f, counter, "/", flen )
    label = files[f[-14:]]["label"]
    if label == "FAKE":
      original = files[f[-14:]]["original"]
      try:
        frame = getFakestFrames('./input/' + original, f, 35)
      except:
        frame = getFrame100(f)
    else:
      doImage(f, counter, 1)
      frame = take5Frames(f, i)

    if not isinstance(frame, list):
      frame = getFrame100(f)  
  frame = preprocessImage(frame);


  set_used = "train"
  if(counter > validation_threshold):
    set_used = "validation"
  if not os.path.exists('./output/'+set_used+'/'):
    os.makedirs('./output/'+set_used+'/')
    os.makedirs('./output/'+set_used+'/fake/')
    os.makedirs('./output/'+set_used+'/real/')
  if label == "FAKE":
    cv2.imwrite( "./output/"+set_used+"/fake/"+f[-14:]+ ""+ str(i) +".jpg", frame)
  else: 
    cv2.imwrite( "./output/"+set_used+"/real/"+f[-14:]+ ""+ str(i) +".jpg", frame) 

if __name__ == "__main__":
    run()


