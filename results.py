import numpy as np
import math
import sklearn.metrics

#labels = np.loadtxt('labels.txt')
#predictions = np.loadtxt('predictions.txt')
pred_prob = np.loadtxt('probabilities.txt')
testfiles = glob.glob('./test_videos/*.mp4')

with open('metadata.json') as json_file:
    files = json.load(json_file)
labels = []
predictions = []
counter = 0
print(len(testfiles))
for f in testfiles:
  label = files[f[-14:]]["label"]
  if label == "FAKE":
    labels.append(0)
  else:
    labels.append(1)

  if(pred_prob[counter] >= 0.5):
    pred = 1
  else:
    pred = 0

  predictions.append(pred)
  counter += 1  


print(labels)
print(predictions)

mse = sklearn.metrics.mean_squared_error(labels, predictions)
print('Mean Squared Error = '+str(mse))

accuracy = sklearn.metrics.accuracy_score(labels, predictions)
print('accuracy = '+str(accuracy))

precision = sklearn.metrics.precision_score(labels ,predictions)
print('precision_score = '+str(precision))

recall = sklearn.metrics.recall_score(labels ,predictions)
print('recall_score = '+str(recall))

f1 = sklearn.metrics.f1_score(labels ,predictions)
print('f1_score = '+str(f1))

conf = sklearn.metrics.confusion_matrix(labels, predictions)
print(conf)


loss = 0;
for x in range(len(labels)):
  probfake = 1-pred_prob[x]
  loss += predictions[x] * math.log(probfake) + (1 - predictions[x]) * math.log(1-probfake)

loss = loss * (-1/len(labels))
print(loss)