**DeepFake Detection**

Run balanced.py to preprocess the videos which are listed in the input folder. 
This will create an ouput folder with a train and validation set based on the input amount of videos, with preprocessed images.

After preprocessing we can run, clone from
https://github.com/nii-yamagishilab/Capsule-Forensics-v2.git

`python ./Capsule-Forensics-v2/train_binary_ffpp.py --dataset ./output --outf ./chps/ `

Which will output

Namespace(batchSize=32, beta1=0.5, dataset='./', disable_random=False, dropout=0.001, gpu_id=0, imageSize=300, lr=0.0005, manualSeed=None, niter=25, outf='./chps/', resume=0, train_set='train', val_set='validation', workers=0)

Random Seed:  9230

100% 116/116 [00:52<00:00,  1.04it/s]

[Epoch 1] Train loss: 2.4110   acc: 79.39 | Test loss: 2.6882  acc: 54.86
...
...


After training we can run

`!python ./Capsule-Forensics-v2/test_binary_ffpp.py --dataset ./ --outf ./chps/ --id 23`

Output:

`Namespace(batchSize=32, dataset='./', gpu_id=0, id=23, imageSize=300, outf='./chps/', random=False, test_set='test', workers=0)`

`100% 8/8 [00:02<00:00,  2.91it/s]`

`[Epoch 23] Test acc: 73.60   EER: 35.00`



Running results.py to generate the test results:
Mean Squared Error = 0.2571428571428571

accuracy = 0.7428571428571429

precision_score = 0.2

recall_score = 0.6666666666666666

f1_score = 0.30769230769230765

[[24  8]

 [ 1  2]]
 
log_loss score= 0.8658819620127146
